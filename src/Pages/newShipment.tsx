import "../index.css";
import { SubmitHandler, useForm } from "react-hook-form";
import ky from "ky";
import Avatar from "@mui/material/Avatar";
import LocalShipping from "@mui/icons-material/LocalShipping";
import {
  Box,
  Container,
  Grid,
  ThemeProvider,
  Typography,
  createTheme,
} from "@material-ui/core";
import TextField from "@mui/material/TextField";
import { Button } from "@mui/material";
import { useAppSelector } from "../hooks/hooks";

export interface IShipmentFiled {
  title: string;
  description: string;
  deliveryCost: string;
}

const defaultTheme = createTheme();

export default function NewShipment() {
  const {
    register,
    formState: { errors, isValid },
    handleSubmit,
  } = useForm<IShipmentFiled>({
    mode: "onBlur",
  });
  const token = useAppSelector((state) => state.user.user.token);
  const onSubmit: SubmitHandler<IShipmentFiled> = async (data) => {
    //alert(JSON.stringify(data));
    await ky
      .post("http://localhost:5050/ordering/v1/Orders", {
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
        json: {
          title: data.title,
          description: data.description,
          deliveryCost: data.deliveryCost,
        },
        mode: "cors",
      })
      .json()
      .then(console.log);
    //reset();
  };

  return (
    <ThemeProvider theme={defaultTheme}>
      <Container component="main" maxWidth="xs">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography component="h1" variant="h5">
            Новый Заказ
          </Typography>
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LocalShipping />
          </Avatar>

          <Box
            component="form"
            onSubmit={handleSubmit(onSubmit)}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  {...register("title", {
                    required: true,
                    minLength: {
                      value: 2,
                      message: "Минимум 2 символов!",
                    },
                  })}
                  error={!!errors?.title}
                  autoComplete="Title"
                  required
                  fullWidth
                  label="Краткий заголовок"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  {...register("description", {
                    required: "Поле обязтельно к заполнению!",
                    minLength: {
                      value: 10,
                      message: "Минимум 10 символов!",
                    },
                  })}
                  error={!!errors?.description}
                  required
                  fullWidth
                  label="Описание Доставки"
                  autoComplete="family-name"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  {...register("deliveryCost", {
                    required: "Поле обязтельно к заполнению!",
                    minLength: {
                      value: 1,
                      message: "Минимум 1 символ!",
                    },
                  })}
                  error={!!errors?.deliveryCost}
                  required
                  fullWidth
                  label="Стоимость доставки"
                  autoComplete="family-name"
                />
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                disabled={!isValid}
              >
                Добавить
              </Button>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}
