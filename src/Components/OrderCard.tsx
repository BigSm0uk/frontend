import {
  Grid,
  Card,
  CardHeader,
  Avatar,
  CardContent,
  Typography,
  CardActions,
  Button,
  Collapse,
  Box,
} from "@mui/material";
import { red } from "@mui/material/colors";
import { IOrder } from "../interfaces/appInterfaces";
import { useState } from "react";
import CurrencyRubleIcon from "@mui/icons-material/CurrencyRuble";
import { useCardSyles } from "../Styles/CardStyle";
import ky from "ky";
import { useAppSelector } from "../hooks/hooks";

export interface OrderProps extends IOrder {
  expanded: boolean;
}

export const OrderList = (orderFromProps: IOrder) => {
  //const ordersFromRx = useAppSelector((state) => state.order.order.orders!);
  const { card, action } = useCardSyles();
  const [order, setOrder] = useState(orderFromProps);
  const [isExpanden, setExpanded] = useState(false);
  const token = useAppSelector((state) => state.user.user.token);
  return (
    <Grid item xs={12} md={12} lg={12}>
      <Card variant="outlined" className={card}>
        <CardHeader
          avatar={
            <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
              O
            </Avatar>
          }
          title="User name"
          subheader="September 14, 2016"
        />
        <CardContent>
          <Typography variant="body2">{order.title}</Typography>
        </CardContent>
        <CardActions disableSpacing className={action}>
          <Button
            sx={{ marginRight: "auto" }}
            size="small"
            color="success"
            onClick={async () => {
              await ky
                .post(
                  `http://localhost:5050/shipping/v1/Deliveries/${order.id}`, //убрать id в query (путает)
                  {
                    headers: {
                      Authorization: "Bearer " + token,
                    },
                  }
                )
                .json()
                .then(console.log);
            }}
          >
            Взять в работу
          </Button>
          <Box>
            <Typography>
              {order.deliveryCost} <CurrencyRubleIcon />
            </Typography>
          </Box>
          <Button
            size="small"
            onClick={async () => {
              setExpanded(!isExpanden);
              if (!order.description)
              await ky
                .get(
                  //`http://localhost:5050/shipping/v1/Orders/${order.id}`,
                  `http://localhost:5050/ordering/v1/HotOrders/${order.id}`,
                  {
                    headers: {
                      Authorization: "Bearer " + token,
                    },
                  }
                )
                .json()
                .then(response => setOrder(response as IOrder));
            }}
          >
            Подробнее
          </Button>
        </CardActions>
        <Collapse in={isExpanden} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>Описание:</Typography>
            <Typography paragraph>{order.description}</Typography>
          </CardContent>
        </Collapse>
      </Card>
    </Grid>
  );
};
