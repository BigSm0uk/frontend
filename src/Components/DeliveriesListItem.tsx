import { useState } from "react";
import {
  Grid,
  Card,
  CardHeader,
  Avatar,
  CardContent,
  Typography,
  CardActions,
  Button,
  Collapse,
  Box,
} from "@mui/material";
import { red } from "@mui/material/colors";
import { useCardSyles } from "../Styles/CardStyle";
import CurrencyRubleIcon from "@mui/icons-material/CurrencyRuble";
import ky from "ky";
import { useAppSelector } from "../hooks/hooks";
import { IDelivery } from "../interfaces/appInterfaces";

export const DeliveriesListItem = (deliveryFromProps: IDelivery) => {
  const { card, action } = useCardSyles();
  const [{ id, status, orderDescription, cost }, setDelivery] =
    useState(deliveryFromProps);
  const [isExpanden, setExpanded] = useState(false);
  const token = useAppSelector((state) => state.user.user.token);

  // TODO: вынести http запросы apiClient
  const getDelivery = async (id: string) => {
    await ky
      .get(`http://localhost:5050/shipping/v1/Deliveries/${id}`, {
        headers: {
          Authorization: "Bearer " + token,
        },
      })
      .json()
      .then((response) => setDelivery(response as IDelivery));
  };

  const updateDelivery = async (newStatus: string) => {
    await ky
      .put(
        `http://localhost:5050/shipping/v1/Deliveries/${id}?status=${newStatus}`,
        {
          headers: {
            Authorization: "Bearer " + token,
          },
        }
      )
      .json()
      .then(() =>
        setDelivery({ id, status: newStatus, orderDescription, cost })
      );
  };

  return (
    <Grid item xs={12} md={12} lg={12}>
      <Card variant="outlined" className={card}>
        <CardHeader
          avatar={
            <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
              O
            </Avatar>
          }
          title="User name"
          subheader="September 14, 2016"
        />
        <CardContent>
          <Typography variant="body2">
            {orderDescription.substring(0, 40)} : {status}
          </Typography>
        </CardContent>
        <CardActions disableSpacing className={action}>
          <Button
            disabled={status !== "Created"}
            sx={{ marginRight: "auto" }}
            size="small"
            color="success"
            onClick={async () => {
              console.log("starting delivery...");
              await updateDelivery("Started");
            }}
          >
            Начать доставку
          </Button>
          <Button
            disabled={status !== "Started"}
            sx={{ marginRight: "auto" }}
            size="small"
            color="success"
            onClick={async () => {
              await updateDelivery("Finished");
            }}
          >
            Завершить доставку
          </Button>
          <Box>
            <Typography>
              {cost} <CurrencyRubleIcon />
            </Typography>
          </Box>
          <Button
            size="small"
            onClick={async () => {
              setExpanded(!isExpanden);
              if (!orderDescription) getDelivery(id);
            }}
          >
            Подробнее
          </Button>
        </CardActions>
        <Collapse in={isExpanden} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>Описание:</Typography>
            <Typography paragraph>{orderDescription}</Typography>
          </CardContent>
        </Collapse>
      </Card>
    </Grid>
  );
};
